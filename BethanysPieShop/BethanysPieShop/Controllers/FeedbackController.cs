﻿using BethanysPieShop.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BethanysPieShop.Controllers
{
    [Authorize]
    public class FeedbackController : Controller
    {
        private readonly IFeedbackRepository feedbackRepository;

        public FeedbackController( IFeedbackRepository feedbackRepositoryParam )
        {
            feedbackRepository = feedbackRepositoryParam;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index( Feedback feedback )
        {
            if ( ModelState.IsValid )
            {
                feedbackRepository.AddFeedback( feedback );
                return RedirectToAction( "FeedbackComplete" );
            }
            else
            {
                return View( feedback );
            }
        }

        public IActionResult FeedbackComplete()
        {
            return View();
        }
    }
}