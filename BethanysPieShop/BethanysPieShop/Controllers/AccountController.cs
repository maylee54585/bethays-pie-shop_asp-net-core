﻿using BethanysPieShop.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BethanysPieShop.Controllers
{
    public class AccountController : Controller
    {
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly UserManager<IdentityUser> userManager;

        public AccountController( SignInManager<IdentityUser> signInManagerParam, UserManager<IdentityUser> userManagerParam )
        {
            signInManager = signInManagerParam;
            userManager = userManagerParam;
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login( LoginViewModel loginViewModel )
        {
            if ( !ModelState.IsValid )
            {
                return View( loginViewModel );
            }

            var user = await userManager.FindByNameAsync( loginViewModel.UserName );

            if ( user != null )
            {
                var result = await signInManager.PasswordSignInAsync( user, loginViewModel.Password, false, false );

                if ( result.Succeeded )
                {
                    return RedirectToAction( "Index", "Home" );
                }
            }

            ModelState.AddModelError( "", "User name / password not found." );

            return View( loginViewModel );
        }

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register( LoginViewModel loginViewModel )
        {
            if ( ModelState.IsValid )
            {
                var user = new IdentityUser()
                {
                    UserName = loginViewModel.UserName
                };

                var result = await userManager.CreateAsync( user, loginViewModel.Password );

                if ( result.Succeeded )
                {
                    return RedirectToAction( "Index", "Home" );
                }
            }

            return View( loginViewModel );
        }

        [HttpPost]
        public async Task<ActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction( "Index", "Home" );
        }
    }
}