﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BethanysPieShop.Models;
using BethanysPieShop.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace BethanysPieShop.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPieRepository _pieRepository;

        public HomeController(IPieRepository pieRepositoryParam)
        {
            _pieRepository = pieRepositoryParam;
        }


        public IActionResult Index()
        {
            var homeViewModel = new HomeViewModel
            {
                Title = "Welcome to Bethany's Pie Shop",
                Pies = _pieRepository.GetAllPies().OrderBy(p=> p.Name).ToList()
            };

            return View(homeViewModel);
        }

        public IActionResult Details(int id)
        {
            var pie = _pieRepository.GetPieById(id);
            if (pie == null)
                return NotFound();

           return View(pie);
        }
    }
}