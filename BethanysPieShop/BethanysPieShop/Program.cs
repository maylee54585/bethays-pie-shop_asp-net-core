﻿using BethanysPieShop.Models;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace BethanysPieShop
{
    /// <summary>
    /// Asp.Net is infact a Console Application.
    /// </summary>
    public class Program
    {
        // All Console applications have a Main Function
        public static void Main( string[] args )
        {
            
            var host = CreateWebHostBuilder( args ).Build();

            using ( var scope = host.Services.CreateScope() )
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<AppDbContext>();
                    DbInitializer.Seed( context );
                }
                catch ( Exception ex )
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError( ex, "An error occurred while seeding the database." );
                }
            }

            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder( string[] args ) =>
            // Configuration of Kestrel Web Server
            WebHost.CreateDefaultBuilder( args )
                .UseStartup<Startup>();
    }
}