﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.ComponentModel.DataAnnotations;

namespace BethanysPieShop.Models
{
    public class Feedback
    {
        [BindNever]
        public int FeedbackId { get; set; }

        [Required]
        [StringLength( 100, ErrorMessage = "Your name is required." )]
        public string Name { get; set; }

        [Required]
        [StringLength( 100, ErrorMessage = "Your email is required." )]
        [DataType( DataType.EmailAddress )]
        [RegularExpression( @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
+ "@"
+ @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$", ErrorMessage = "Email doesn't appear to be a valid Email Address" )]
        public string Email { get; set; }

        [Required]
        [StringLength( 5000, ErrorMessage = "Message is required." )]
        public string Message { get; set; }

        public bool ContactMe { get; set; }
    }
}