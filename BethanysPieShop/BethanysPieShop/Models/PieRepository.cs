﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BethanysPieShop.Models
{
    public class PieRepository : IPieRepository
    {
        private readonly AppDbContext _appDbContext;

        public PieRepository(AppDbContext appDbContextParam)
        {
            _appDbContext = appDbContextParam;
        }
        public IEnumerable<Pie> GetAllPies()
        {
            return  _appDbContext.Pies;
        }

        public Pie GetPieById( int pieIdParam )
        {
            return _appDbContext.Pies.FirstOrDefault(p => p.Id == pieIdParam);
        }
    }
}
