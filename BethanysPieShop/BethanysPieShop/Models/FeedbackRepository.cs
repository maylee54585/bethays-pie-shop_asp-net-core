﻿namespace BethanysPieShop.Models
{
    public class FeedbackRepository : IFeedbackRepository
    {
        private readonly AppDbContext _appDbContext;

        public FeedbackRepository( AppDbContext appDbContextParam )
        {
            _appDbContext = appDbContextParam;
        }

        public void AddFeedback( Feedback feedbackParam )
        {
            _appDbContext.Feedbacks.Add( feedbackParam );
            _appDbContext.SaveChanges();
        }
    }
}