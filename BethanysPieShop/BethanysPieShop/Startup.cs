﻿using BethanysPieShop.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BethanysPieShop
{
    /// <summary>
    /// Configuration of Web Application
    /// Define the Http request pipeline and Configure all services.
    /// </summary>
    public class Startup
    {
        // Automatically reads appsettings.json file
        public IConfiguration Configuration { get; }

        public Startup( IConfiguration configurationParam )
        {
            Configuration = configurationParam;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940

        // Asp.Net Core uses a Dependency Injection Principle.
        // We will be registering services (System & User) in this function.
        public void ConfigureServices( IServiceCollection services )
        {
            // Add Entity Framework
            services.AddDbContext<AppDbContext>( options => options.UseSqlServer( Configuration.GetConnectionString( "DefaultConnection" ) ) );


            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<AppDbContext>();

            // When someone ask s for IPieRepository then a new MockPieRepository is returned every time.
            services.AddTransient<IPieRepository, PieRepository>();
            services.AddTransient<IFeedbackRepository, FeedbackRepository>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        // Middleware which intercepts Http request and response
        // ! Sequence is important !
        public void Configure( IApplicationBuilder app, IHostingEnvironment env )
        {
            //if ( env.IsDevelopment() )
            //{
            //    app.UseDeveloperExceptionPage();
            //}

            //app.Run( async ( context ) =>
            // {
            //     await context.Response.WriteAsync( "Hello World!" );
            // } );

            app.UseDeveloperExceptionPage();
            app.UseStatusCodePages();
            app.UseStaticFiles();               // return static files wwwroot folder
            app.UseAuthentication();
            app.UseMvc( routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}"
                    );
            } );
        }
    }
}